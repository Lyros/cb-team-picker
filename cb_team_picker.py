#!/usr/bin/env python3

import argparse
import csv
import sys

def parse_args():
    parser = argparse.ArgumentParser(
            description = 'Calculate optimal teams for Clan Battle.')

    parser.add_argument('filename',
            help = 'CSV file to use')
    parser.add_argument('-c', '--count',
            default = 5,
            type = int,
            help = 'Number of results to display')
    parser.add_argument('-s', '--sort',
            default = 'score',
            choices = ['score', 'damage'],
            help = 'How results are sorted')
    parser.add_argument('-e', '--exclude',
            nargs = '*',
            help = 'Bosses to exclude. Accepts either the boss name or number.\
                    Example: --exclude Wyvern 2 3')
    parser.add_argument('-m', '--missing',
            nargs = '*',
            help = "Characters you are missing or don't have leveled and must \
                    be borrowed. Example: --missing Jun Illya")
    parser.add_argument('--hits',
            nargs = '*',
            help = "Require a certain number of hits on bosses. \
                    Expects a Boss name followed by the number of hits, \
                    repeated for multiple bosses as needed. \
                    Example: --hits Orc 1 Wyvern 1")

    return parser.parse_args()

def pair(lst):
    for i in range(0, len(lst), 2):
        yield lst[i:i + 2]

def check_hits_param():
    if not args.hits:
        return
    if len(args.hits) % 2 != 0:
        sys.exit('"--hits" requires an even number of parameters, exiting')

    global required_hits
    required_hits = {}
    total_hits = 0
    for boss, num_hits in pair(args.hits):
        boss = bosses.index(boss.title())
        num_hits = int(num_hits)
        required_hits[boss] = num_hits
        if num_hits <= 0:
            sys.exit('Number of hits on a boss must greater than 1')
        total_hits += num_hits

    if total_hits > 3:
        sys.exit('Number of total hits must be no greater than 3')

def setup(filename):
    with open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile)
        global bosses
        bosses = [x.title() for x in next(reader)]
        bosses = list(filter(None, bosses)) # Remove empty strings

        score_multipliers = next(reader)
        score_multipliers = list(filter(None, score_multipliers))
        if len(score_multipliers) != len(bosses):
            sys.exit(f'First and second lines must have an equal number of fields, '
                    f'got {len(score_multipliers)} and {len(bosses)}, respectively')
        score_multipliers = [float(i) for i in score_multipliers]

        # Desired structure
        # {boss: 0-4, damage: number, score: number,
        # team: [array, of, characters, in, team]}
        comps = list()
        for row in reader:
            comp = {'boss': None, 'damage': None, 'score': None, 'team': None}
            comp['boss'] = bosses.index(row[0].title())
            comp['damage'] = int(float(row[1]))
            comp['score'] = int(score_multipliers[comp['boss']] * comp['damage'])
            comp['team'] = [x.title() for x in row[2:7]]
            comps.append(comp)

        return comps

def handle_exclude(comps, exclude):
    if exclude is None:
        return comps
    for boss in exclude:
        if boss.isdigit():
            boss = int(boss) - 1
            if boss not in range(0, len(bosses)):
                print(f'WARNING: there is no boss {boss+1}, skipping')
                continue
        else:
            boss = boss.title()
            if boss not in bosses:
                print(f'WARNING: unknown boss "{boss}", skipping')
                continue
            boss = bosses.index(boss)

        comps = [x for x in comps if x['boss'] != boss]
    return comps

# Checks the number of borrowed characters
# To make this easier, we cheat: The game allows 1 character to be borrowed, or
# in other words, 4 characters will be used up from our roster per hit. By
# counting how many unique characters we used up, we know if we borrowed too
# many characters.
# The --missing parameter makes things more complicated, but basically, if a
# missing char is in a team, remove it from the roster simualate it being
# borrowed. Also check that we don't have two or more missing chars in a single
# team.
def is_valid_combination(teams):
    limit = len(teams) * 4
    missing = list()
    marked = list()
    if args.missing is not None:
        missing = [x.title() for x in args.missing]

    roster = set()
    for x in teams:
        for y in missing:
            if y in x:
                marked.append(y)

        if len(marked) >= 2:
            # Two or more missing chars in team
            return False
        marked = list()
        roster.update(set(x))
    roster.difference_update(set(missing))
    if len(roster) < limit:
        return False
    return True

def calc_optimal_hits(comps):
    valid = list()
    for i, first_team in enumerate(comps):
        for j, second_team in enumerate(comps):
            if j <= i: continue
            if not is_valid_combination([first_team['team'], second_team['team']]): continue
            for k, third_team in enumerate(comps):
                if k <= j: continue
                if not is_valid_combination([first_team['team'], third_team['team']]): continue
                if not is_valid_combination([second_team['team'], third_team['team']]): continue
                if not is_valid_combination([first_team['team'], second_team['team'], third_team['team']]): continue
                valid.append({
                    'damage': first_team['damage'] + second_team['damage'] + third_team['damage'],
                    'score': first_team['score'] + second_team['score'] + third_team['score'],
                    'comps': [first_team, second_team, third_team]
                    })

    valid.sort(key=lambda i: i[args.sort], reverse=True)
    return valid

def filter_hits(results):
    if not required_hits:
        return results

    marked_for_removal = list()
    for i, result in enumerate(results):
        boss_counter = dict.fromkeys(required_hits.keys(), 0)
        for comp in result['comps']:
            if comp['boss'] in required_hits.keys():
                boss_counter[comp['boss']] += 1
        if boss_counter != required_hits:
            marked_for_removal.append(i)

    marked_for_removal.sort(reverse=True)
    for index in marked_for_removal:
        del results[index]

    return results

def print_results(results):
    if not results:
        sys.exit('No results found')

    for c, result in enumerate(results):
        if c >= args.count:
            break
        print(f'Total Damage:  {result["damage"]:<10,} Total Score: {result["score"]:<,}')
        for i, comp in enumerate(result['comps']):
            print(f'Hit {i+1}: Damage: {comp["damage"]:<16,} '
                    f'Score: {comp["score"]:<12,} '
                    f'Boss: {bosses[comp["boss"]]:<14} '
                    f'Team: {", ".join(comp["team"])}')
        print()  # New line between results


bosses = None
required_hits = None
args = parse_args()
comps = setup(args.filename)
check_hits_param()
comps = handle_exclude(comps, args.exclude)
results = calc_optimal_hits(comps)
results = filter_hits(results)

print_results(results)
