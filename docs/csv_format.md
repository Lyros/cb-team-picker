# Expected CSV Format
This is a description of what the script expects as input.
You can find examples in the `csv/` folder.
There are 3 types of lines:
- Boss Name line
- Score Multiplier line
- Team Composition line

The CSV must start with the Boss Name line, followed by the Boss score multiplier line, followed by three or more Team composition lines

### Boss Name line
Contains boss names.
Amount doesn't matter, as long as it equals to the amount in the score multiplier line.
Order should be the in-game order, but doesn't have to be, as long as the bosses correspond to the correct Score multiplier line.

### Score Multiplier line
Contains the score multipliers of the bosses.
Amount doesn't matter, as long as it equals to the amount in the boss name line.

### Team Composition line

Must be in the following format:  
`Boss,Damage,Character_1,Character_2,Character_3,Character_4,Character_5`  

- `Boss` must be one of the 5 bosses from the Boss Name line.
- `Damage` must be a number.
Cannot contain any symbols or letters, except `e` or `E` which allows for scientific notation.
For example, `1E6` equals 1x10^6 equals one million.
- `Character_#` should be the name of a character in the game.
I have chosen to standardize to the in-game spelling, but as long as you are consistent across the csv file, it doesn't matter.
