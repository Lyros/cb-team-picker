## Requirements
Python 3.6+

## Setup
Obtain via ZIP download (above), or `git clone https://gitlab.com/Lyros/cb-team-picker.git`

## Usage
![CB Team Picker Screenshot](docs/screenshot.png)
Basic Usage:
```
python cb_team_picker.py csv/cb2108.csv
```

Assume Illya and Hiyori needs to be borrowed:
```
python cb_team_picker.py csv/cb2108.csv --missing Hiyori Illya
```

Exclude the third boss:
```
python cb_team_picker.py csv/cb2108.csv --exclude 3
```

Require one hit on Wyvern and one hit on Orc:
```
python cb_team_picker.py csv/cb2108.csv --hits Orc 1 Wyvern 1
```

See help for more options:
```
python cb_team_picker.py -h
```

## Expected CSV format
[Expected CSV format](docs/csv_format.md)
